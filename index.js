//require directive tells us to load the express module
const express = require('express');

//creating a server using the express
const app = express();

//port
const port = 4000;

//midlewares
app.use(express.json());
	//allows app to read a json data

app.use(express.urlencoded({extended: true}));
	//allows app to read from forms
	//by default, information received from the url can only be received as string or an array
	//with extended: true, this allows to receive information in other data types such as objects.

//mock database

let users = [
	{
		email: "nezukoKamado@gmail.com",
		userName:"nezuko01",
		password:"letMeOut",
		isAdmin: false
	},
	{
		email: "tanjiroKamado@gmail.com",
		userName:"gonpanchiro01",
		password:"iAmTanjiro",
		isAdmin: false
	},
	{
		email: "zenitsuAgatsuma@gmail.com",
		userName:"zenitsuSleeps",
		password:"iNeedNezuko",
		isAdmin: true
	}
]


let loggedUser;

//GET Method
app.get('/', (req, res) => {
	res.send('Hello World')
});

//app - server 
//get - HTTP method
// '/' - route name or endpoint
//(req,res) - request and response - will handle requests and the responses
//res.send - combines writeHead() and .end(), used to send response to our client

/*
	Mini-activity:

	Make a route with /hello as an endpoint.
	Send a message that says "Hello from Batch 131"
*/
app.get('/hello', (req, res) => {
	res.send('Hello from Batch 131')
});


//POST Method
app.post('/', (req, res) => {
	console.log(req.body);
	res.send('Hello I am POST method')
});

//we use console.log to check what's inside the request body.

/*
	Mini-activity:

	Change the message that we send: "Hello I am <name>, I am <age>. I could be described as <description>"

	solution:
	app.post('/', (req, res) => {
	console.log(req.body);
	res.send(`Hello I am ${req.body.name}, I am ${req.body.age}. I could be described as ${req.body.description}`)
});

*/

app.post('/users/register', (req,res) =>{
	console.log(req.body);

	let newUser = {
		email: req.body.email,
		userName: re.body.userName,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	users.push(newUser);
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered.`)
});

//login route

app.post('/users/login', (req, res) => {
	console.log(req.body);

	let foundUser = users.find((user) => {

		return user.userName === req.body.userName && user.password === req.body.password;
	});

	if(foundUser !== undefined) {

		let foundUserIndex = users.findIndex((user) => {

			return user.userName === foundUser.userName
		});

		foundUser.index = foundUserIndex;

		loggedUser = foundUser;

		console.log(foundUser)

		res.send('Thank you for logging in.')
		
	} else {
		loggedUser = foundUser;

		res.send('Login Failed, wrong credentials.')
	}
})

//Change-Password Route

app.put('/users/change-password', (req, res) => {


	//to store the message that  will be sent back to our client
	let message;


	//will loop through all the "users" array
	for(let i=0; i < users.length; i++){


		//if the username provided in the request is the same with the username in the loop
		if(req.body.username === users[i].username){

			//change the password of the user found in the loop by the requested password in the body by the client
			users[i].password = req.body.password;

			//send a message to the client
			message = `User ${req.body.userName}'s password has been changed.`;

			//break the loop once a user matches the username provided in the client
			break;
			
		//if no user was found
		} else {

			//changes the message to be sent back as a response
			message = `User not found.`
		}
	}

	//response that will be sent to our client.
	res.send(message)
})


//Activity

app.get('/home', (req, res) => {
	res.send('Welcome')
});

app.get('/users', (req, res) => {
	res.send(users);
});



//listen to the port and returning  message in the terminal.
app.listen(port, () => console.log(`Server is running at port ${port}`));